#!/bin/sh

usage() {
	cat << EOF

Usage: $0 COMMAND

Commands:
  add    [project-id] [mr-iid | branch-name remote_url]
         Add a worktree for a given MR
EOF

	exit 1
}

uri_encode() {
	jq -rn --arg x "$1" '$x|@uri'
}

infer_gitlab_project_of_branch() {
	branch="$1"

	remote=$(git config --get branch."$branch".pushRemote 2> /dev/null)
	if [ -z "$remote" ]; then
		remote=$(git config --get branch."$branch".remote 2> /dev/null)
	fi
	if [ -n "$remote" ]; then
		git remote get-url "$remote" 2> /dev/null | sed 's/.*gitlab\.com:\(.*\)\.git/\1/'
	fi
}

infer_gitlab_project() {
	project=$(infer_gitlab_project_of_branch "master")
	if [ -z "$project" ]; then
		project=$(infer_gitlab_project_of_branch "main")
	fi
	echo "$project"
}

add_mr_worktree() {
	mr_iid="${1:-}"
	case "$mr_iid" in
	'' | *[!0-9]*)

		echo "Usage: $0 add [MR_IID]"
		return 1
		;;

	esac

	cd "$(git rev-parse --show-toplevel)" || return 1

	## TODO: we can ignore use the refs/merge-requests/ to simplify all of the below:
	dir_prefix="${mr_iid}-"
	mr_file=$(mktemp)
	if ! curl --silent --fail "https://gitlab.com/api/v4/projects/$(uri_encode "${project_id}")/merge_requests/${mr_iid}" > "$mr_file"; then
		echo "Could not find MR !${1}."
		return 12
	fi

	branch_name=$(jq -r .source_branch < "$mr_file")
	source_project_id=$(jq -r .source_project_id < "$mr_file")
	rm "$mr_file"
	remote_url=$(curl -s "https://gitlab.com/api/v4/projects/${source_project_id}" | jq -r .ssh_url_to_repo)

	if [ -z "$remote_url" ]; then
		echo "Could not find the remote url"
		return 1
	fi

	remote=$(echo "$remote_url" | sed 's/.*gitlab\.com:\(.*\)\/.*\.git/\1/')
	if [ -z "$remote" ]; then
		echo "Could not deduce the remote from remote_url $remote_url"
		return 1
	fi

	if ! git remote get-url "$remote" > /dev/null; then
		git add remote "$remote" "$remote_url" --guess-remote
	fi

	dir_name=../${dir_prefix}$(echo "${branch_name}" | tr "/" "_")
	if [ -d "$dir_name" ]; then
		echo "$dir_name already exists, going there"
		cd "$dir_name" || return 1
		return 11
	fi

	git fetch "$remote" "${branch_name}" || {
		echo "Could not fetch branch ${branch_name} at $remote_url"
		return 1
	}
	git worktree add "$dir_name" "$branch_name"
	cd "$dir_name" || return 1
}

gitlab_worktree() {
	if [ -n "${TRACE:-}" ]; then set -x; fi

	if [ "${1:-}" = "--help" ]; then
		usage
	fi

	if ! { [ -d .git ] || [ -f .git ]; }; then
		echo "Must be run from a git repository."
		return 1
	fi

	if [ -n "$PROJECT_ID" ]; then
		project_id=${PROJECT_ID}
	else
		default_project_id=$(infer_gitlab_project || echo "")
		if [ -n "$default_project_id" ]; then
			project_id=$default_project_id
		else
			echo "PROJECT_ID not set and could not deduce GitLab project from git remotes. See '$0 --help'." >&2
			return 1
		fi
	fi

	command=${1:-}
	case $command in
	add)
		shift
		add_mr_worktree "$@"
		;;
	"project")
		shift
		echo "$project_id"
		;;
	*)
		echo "Unrecognized command '${command}'. See '$0 --help'."
		;;

	esac
}
