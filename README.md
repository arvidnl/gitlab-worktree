# `gitlab-worktree`

A shell function to work with [git
worktrees](https://git-scm.com/docs/git-worktree) corresponding to
GitLab merge requests.

## Prerequisites

`gitlab-worktree` requires `curl` and `jq`.

## Installation

Clone this repository and source `gitlab-worktree.sh` somewhere in your shell configuration:

```
git clone https://gitlab.com/arvidnl/gitlab-worktree.git
echo "source \"gitlab-worktree/gitlab-worktree.sh\"" >> ~/.bashrc
```

Restart your shell or reload your shell configuration.
    
## Usage

Use the entrypoint command `gitlab_worktree`. It supports the
following commands:

 - `add <MR_ID>`: Add a worktree tracking a given merge
   requests.

   Example: `gitlab_worktree add !1234` will create and change
   directory to the worktree `../1234-<mr_branch>` where `<mr_branch>`
   is source branch of merge request `!1234`.

## Project inference
 
`gitlab_worktree` tries to infer the GitLab project corresponding to
the git repository in the working directory by looking at the
following remotes, taking the first that is defined:

 - the `pushRemote` of the `master` branch.
 - the `remote` of the `master` branch.
 - the `pushRemote` of the `main` branch.
 - the `remote` of the `main` branch.

Project inference can be overridden by setting the `PROJECT_ID`
environment variable.

## Limitations
 
 - Does support private repositories.
 - Does not support hosted GitLab instances.

